FROM ubuntu:trusty
MAINTAINER Stephen Thirlwall <sdt@dr.com>

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        autoconf \
        build-essential \
        curl \
        gdb \
        git \
        libdbd-mysql-perl \
        libdbi-perl \
        libexiv2-dev \
        libfreetype6-dev \
        libhttp-message-perl \
        libio-socket-inet6-perl \
        libmp3lame-dev \
        libnet-upnp-perl \
        libqt5webkit5-dev \
        libtag1-dev \
        libtool \
        libwww-perl \
        libxinerama-dev \
        pkg-config \
        python-lxml \
        python-mysqldb \
        python-urlgrabber \
        qt5-default  \
        qtscript5-dev \
        qttools5-dev-tools \
        uuid-dev \
        yasm

WORKDIR /build

RUN git clone https://github.com/MythTV/mythtv.git

CMD /bin/bash
