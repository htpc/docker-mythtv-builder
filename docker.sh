IMAGE=mythtv-builder:0.27
CONTAINER=mythtv-builder-container
BRANCH=fixes/0.27
TARBALL=mythtv-$( date +%Y%m%d%H%M%S ).tar.gz

case $1 in

    build)
        docker build -t $IMAGE .
        ;;

    # This builds the docker image and installs all the dependencies.
    run)
        shift
        docker rm -f $CONTAINER
        docker run -ti \
            --name $CONTAINER \
            $IMAGE \
            "$@"
        ;;

    # This updates the fixes/0.27 branch and makes a tarball.
    make)
        docker rm -f $CONTAINER
        docker run \
            --name $CONTAINER \
            $IMAGE \
            bash -c " \
                cd /build/mythtv/mythtv \
                    && git checkout $BRANCH \
                    && git fetch \
                    && git reset --hard $BRANCH \
                    && ./configure --compile-type=release \
                    && make \
                    && make install"
        docker commit $CONTAINER $IMAGE
        docker run --rm $IMAGE tar -czf - /usr/local > $TARBALL
        ;;

    logs)
        docker logs $CONTAINER
        ;;

    stop)
        docker stop $CONTAINER
        ;;

    *)
        echo "usage: $0 [build|logs|run|stop]"
        exit 1

        ;;

esac
